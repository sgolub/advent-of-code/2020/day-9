package main

import (
	"container/list"
	"fmt"

	"gitlab.com/sgolub/advent-of-code/base/v2/days"
)

const dataKey = "program"

type myDay struct {
	data map[string]string
}

func (d myDay) SetData(key, value string) {
	d.data[key] = value
}

// Year will return the AOC Year
func (d myDay) Year() int {
	return 2020
}

// Day will return the day number for this puzzle
func (d myDay) Day() int {
	return 9
}

// GetData will load the data and parse it from disk
func (d myDay) GetData(useSampleData bool) []int {
	result := make([]int, 0)
	data, ok := days.Control().LoadData(d, useSampleData)[dataKey]
	if !ok {
		return result
	}
	for _, i := range data.([]interface{}) {
		result = append(result, i.(int))
	}
	return result
}

type xmas struct {
	queue    *list.List
	preamble int
}

func newXmas(preambleLength int) xmas {
	return xmas{
		preamble: preambleLength,
		queue:    list.New(),
	}
}

func (x *xmas) Contains(num int) bool {
	current := x.queue.Front()
	for {
		if current == nil {
			return false
		} else if current.Value.(int) == num {
			return true
		}
		current = current.Next()
	}
}

func (x *xmas) Valid(num int) bool {
	if x.queue.Len() < x.preamble {
		return true
	}
	n1 := x.queue.Front()
	for {
		if n1 == nil {
			break
		}
		n2 := n1.Next()
		if n2 == nil {
			break
		}
		for {
			if n2 == nil {
				break
			}
			if n1.Value.(int)+n2.Value.(int) == num {
				return true
			}
			n2 = n2.Next()
		}
		n1 = n1.Next()
	}
	return false
}

func (x *xmas) Add(num int) bool {
	if !x.Valid(num) {
		return false
	}
	if x.queue.Len() == x.preamble {
		x.queue.Remove(x.queue.Front())
	}
	x.queue.PushBack(num)
	return true
}

// Solution1 is the solution to the first part of the puzzle
func (d myDay) Solution1(useSample bool) string {
	data := d.GetData(useSample)
	defer days.NewTimer("D9P1")
	preamble := 25
	if useSample {
		preamble = 5
	}
	x := newXmas(preamble)
	for _, n := range data {
		if valid := x.Add(n); !valid {
			return fmt.Sprint(n)
		}
	}
	return "unable to locate"
}

// Solution2 is the solution to the second part of the puzzle
func (d myDay) Solution2(useSample bool) string {
	data := d.GetData(useSample)
	defer days.NewTimer("D9P2")
	preamble := 25
	if useSample {
		preamble = 5
	}
	x := newXmas(preamble)
	firstInvalid := -1
	for _, n := range data {
		if valid := x.Add(n); !valid {
			firstInvalid = n
			break
		}
	}
	if firstInvalid == -1 {
		return "unable to locate invalid"
	}
	var nums []int
	for i, n1 := range data {
		if i >= len(data)-2 {
			break
		}
		if n1 >= firstInvalid {
			continue
		}
		nums = []int{n1}
		sum := n1
		found := false
		for j, n2 := range data[i+1:] {
			if n2 >= firstInvalid {
				continue
			}
			sum += n2
			nums = append(nums, n2)
			if sum == firstInvalid && j >= 1 {
				found = true
				break
			} else if sum > firstInvalid {
				break
			}
		}
		if found {
			break
		}
	}
	smallest := -1
	largest := 0
	for _, i := range nums {
		if i < smallest || smallest == -1 {
			smallest = i
		}
		if i > largest {
			largest = i
		}
	}
	return fmt.Sprint(smallest + largest)
}

// GetDay will return this module's day. Used by the plugin loader.
func GetDay() days.Day {
	return myDay{
		data: map[string]string{},
	}
}
